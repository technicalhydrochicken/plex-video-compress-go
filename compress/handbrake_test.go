package compress_test

import (
	"io/ioutil"
	"testing"

	compress "gitlab.com/technicalhydrochicken/plex-video-compress-go/compress"
)

func TestExtractProgress(t *testing.T) {
	partialOut, _ := ioutil.ReadFile("./test_data/progress_out.txt")
	progress, _ := compress.ExtractProgress(partialOut)

	if *progress.ETASeconds != 47 {
		t.Errorf("Expected ETA of 47 but got %v", progress.ETASeconds)
	}
	if *progress.Progress != 55 {
		t.Errorf("Expected Progress of 55 but got %v", progress.Progress)
	}
	if *progress.Pass != 1 {
		t.Errorf("Expected Pass of 1 but got %v", progress.Pass)
	}
}
