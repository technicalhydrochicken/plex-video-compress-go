package compress

import (
	"bufio"
	"bytes"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type CompressConfig struct {
	Item           string
	ReportInterval int
}

type CompressState struct {
	ETASeconds *int64
	Progress   *int
	Pass       *int64
	PassCount  *int64
}

type CompressResult struct {
	OriginalSize int64
	CompressSize int64
	Duration     time.Duration
}

func ExtractProgress(b []byte) (*CompressState, error) {
	s := &CompressState{}
	br := bytes.NewReader(b)
	scanner := bufio.NewScanner(br)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, "ETASeconds") {
			m := regexp.MustCompile(`\d+`)
			res := m.FindString(line)
			eta, _ := strconv.ParseInt(res, 10, 64)
			s.ETASeconds = &eta
		}
		if strings.Contains(line, "\"Pass\"") {
			m := regexp.MustCompile(`\d+`)
			res := m.FindString(line)
			pass, _ := strconv.ParseInt(res, 10, 64)
			s.Pass = &pass
		}
		if strings.Contains(line, "\"PassCount\"") {
			m := regexp.MustCompile(`\d+`)
			res := m.FindString(line)
			passc, _ := strconv.ParseInt(res, 10, 64)
			s.PassCount = &passc
		}
		if strings.Contains(line, "\"Progress\"") {
			progm := regexp.MustCompile(`\d+\.\d+`)
			progres := progm.FindString(line)
			prog, err := strconv.ParseFloat(progres, 64)
			roundedProg := int(prog * 100)
			if err == nil {
				s.Progress = &roundedProg
			}
		}

	}

	return s, nil

}

func CompressMedia(config CompressConfig) (CompressResult, error) {
	item := config.Item
	c := CompressResult{}
	start := time.Now()

	log.Debug(item)
	log.Debug(GetCompressedMarker(item))
	log.Debug(GetFinalName(item))
	transitionalFilename := GetNormalizingMarker(item)
	origStat, err := os.Stat(item)
	if err != nil {
		return c, err
	}
	c.OriginalSize = origStat.Size()

	app := "HandBrakeCLI"
	cmdArgs := []string{"--json", "--encoder", "x264", "--format", "av_mkv", "--main-feature", "--preset", "Roku 720p30 Surround", "--subtitle", "1,2,3,4,5,6,7,8,9,10", "--audio", "1,2,3,4,5,6,7,8,9,10", "-i", item, "-o", transitionalFilename}
	log.Debug(app, cmdArgs)
	cmd := exec.Command(app, cmdArgs...)
	stdout, _ := cmd.StdoutPipe()
	cmd.Start()
	scanner := bufio.NewScanner(stdout)

	// Get some state variables set up
	var lastETA int64
	var lastProgress int
	var lastProgressShown int
	var lastPass int64
	var lastPassCount int64

	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		m := scanner.Bytes()
		progress, _ := ExtractProgress(m)
		if progress.ETASeconds != nil {
			lastETA = *progress.ETASeconds
		}
		if progress.Pass != nil {
			lastPass = *progress.Pass
		}
		if progress.PassCount != nil {
			lastPassCount = *progress.PassCount
		}
		if progress.Progress != nil {
			lastProgress = *progress.Progress
			// Show the process if needed
			dividend := 1
			if lastProgress%dividend == 0 && lastProgressShown != lastProgress {
				dueETA := time.Second * time.Duration(lastETA)
				log.Printf("%v/%v %v%% %v left", lastPass, lastPassCount, lastProgress, dueETA)
				lastProgressShown = lastProgress
			}
		}
	}
	err = cmd.Wait()
	end := time.Now()
	if err != nil {
		log.Warning("Failed to compress: ", item)
	} else {
		newStat, err := os.Stat(transitionalFilename)
		if err != nil {
			return c, err
		}
		c.CompressSize = newStat.Size()
		c.Duration = end.Sub(start)

		// Remove original file
		err = os.Remove(item)
		if err != nil {
			return c, err
		}

		err = os.Rename(transitionalFilename, GetFinalName(item))
		if err != nil {
			return c, err
		}

		// Rename new file
	}

	return c, nil

}
