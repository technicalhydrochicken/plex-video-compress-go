package compress

import (
	"fmt"
	"os"
	"path"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

func IsMovie(filename string) bool {
	var extension = filepath.Ext(filename)
	movieExtensions := []string{".ts", ".mp4", ".mkv", ".iso", ".img", ".wmv", ".mov", ".avi"}
	return ContainsString(movieExtensions, extension)

}

func BeenCompressed(target string) bool {
	marker := GetCompressedMarker(target)
	if Exists(marker) {
		log.Println(marker)
		return true
	} else {
		return false
	}
}

func GetCompressableMedia(target string) ([]string, error) {
	compressableMediaPaths := []string{}
	err := filepath.Walk(target, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if BeenCompressed(path) {
			log.Printf("Skipping compressed file: %v", path)
			return nil
		}
		if !IsMovie(path) {
			log.Debugf("Skipping non movie file: %v", path)
			return nil
		}
		compressableMediaPaths = append(compressableMediaPaths, path)
		return nil
	})

	if err != nil {
		return nil, err
	}
	return compressableMediaPaths, nil
}

func GetCompressedMarker(filename string) string {
	fspath, file := path.Split(filename)
	newFile := fmt.Sprintf(".compressed.%v.marked", file)
	marker := path.Join(fspath, newFile)
	return marker
}

func GetNormalizingMarker(filename string) string {
	fspath, file := path.Split(filename)
	newFile := fmt.Sprintf("%v.normalizing", file)
	marker := path.Join(fspath, newFile)
	return marker
}

func GetFinalName(filename string) string {
	var extension = filepath.Ext(filename)
	var bareName = filename[0 : len(filename)-len(extension)]
	newFile := fmt.Sprintf("%v.mkv", bareName)
	return newFile
}

func Touch(filename string) error {
	emptyFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	emptyFile.Close()
	return nil
}

func ContainsString(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}

// Exists reports whether the named file or directory exists.
func Exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func CheckErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
