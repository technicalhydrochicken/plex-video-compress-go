package compress_test

import (
	"testing"

	log "github.com/sirupsen/logrus"
	compress "gitlab.com/technicalhydrochicken/plex-video-compress-go/compress"
)

var movieFileMarkers = []struct {
	path   string
	marker string
}{
	{
		"/tmp/bar.mkv",
		"/tmp/.compressed.bar.mkv.marked",
	},
}

var movieFileTests = []struct {
	path    string
	isMovie bool
}{
	{
		"/tmp/bar.log",
		false,
	},
	{
		"/tmp/realmovie.mkv",
		true,
	},
}

func TestIsMovie(t *testing.T) {
	for _, item := range movieFileTests {
		got := compress.IsMovie(item.path)
		if got != item.isMovie {
			t.Errorf("Detected 'isMovie' as %v when it should be %v on %v", got, item.isMovie, item.path)
		}
	}
}

func TestMovieMarker(t *testing.T) {
	for _, item := range movieFileMarkers {
		got := compress.GetCompressedMarker(item.path)
		if got != item.marker {
			t.Errorf("Detected 'GetCompressedMarker' as %v when it should be %v on %v", got, item.marker, item.path)
		}
	}

}

func TestGetCompressableMedia(t *testing.T) {
	compressable, _ := compress.GetCompressableMedia("./test_data/fake_tree")
	if compress.ContainsString(compressable, "test_data/fake_tree/movies/Movie1 (1990)/info.txt") {
		t.Errorf("Found info.txt when searching for only film files")
	}
	if compress.ContainsString(compressable, "test_data/fake_tree/movies/done_movie/done.mkv") {
		t.Errorf("Found already done compressing filme at done.mkv ")
	}
	if !compress.ContainsString(compressable, "test_data/fake_tree/movies/Movie1 (1990)/movie.mkv") {
		log.Println(compressable)
		t.Errorf("Couldn't find compressable file movie.mkv")
	}

}
