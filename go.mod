module gitlab.com/technicalhydrochicken/plex-video-compress-go

go 1.16

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/sirupsen/logrus v1.8.1
)
