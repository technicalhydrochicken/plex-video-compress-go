package main

import (
	"fmt"
	"os"

	"github.com/dustin/go-humanize"
	log "github.com/sirupsen/logrus"
	"gitlab.com/technicalhydrochicken/plex-video-compress-go/compress"
)

func main() {
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	log.SetFormatter(customFormatter)
	customFormatter.FullTimestamp = true
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %v BASE_DIR", os.Args[0])
	}
	targetPath := os.Args[1]

	compressList, err := compress.GetCompressableMedia(targetPath)
	compress.CheckErr(err)

	if len(compressList) == 0 {
		log.Warn("No files to compress")
		os.Exit(0)
	}

	// Print out a header on what we are gonna do
	log.Println("About to compress the following:")
	for index, item := range compressList {
		fmt.Printf("%d: %s\n", index+1, item)
	}

	for _, item := range compressList {
		config := compress.CompressConfig{
			Item:           item,
			ReportInterval: 5,
		}
		r, err := compress.CompressMedia(config)
		if err != nil {
			log.Warning("Could not compress: ", item)
		} else {
			err = compress.Touch(compress.GetCompressedMarker(item))
			if err != nil {
				log.Warning(err)
			} else {
				log.Printf(
					"Shrunk %v from %v to %v in %v",
					item, humanize.Bytes(uint64(r.OriginalSize)), humanize.Bytes(uint64(r.CompressSize)),
					r.Duration)
			}
		}

	}

}
